<?php

class MyCalculator
{

    public $first_val;
    public $second_val;

public function __construct($v_1, $v_2)
{
    $this->first_val = $v_1;
    $this->second_val = $v_2;
}


    public function add(){
        return $this->first_val + $this->second_val;
    }
    public function sub(){
        return $this->first_val - $this->second_val;
    }

    public function mul(){
        return $this->first_val * $this->second_val;
    }
    public function div(){
        return $this->first_val / $this->second_val;
    }



}
$calculate = new MyCalculator(5,6);

echo $calculate->add()."<br>";
echo $calculate->sub()."<br>";
echo $calculate->mul()."<br>";
echo $calculate->div()."<br>";

?>